#include <stdlib.h>
#include <time.h>
#include "../../memoria/virtual/mem_virtual.h"

int main(){
	srand(time(NULL));
	int dado, endereco;
	MemoriaVirtual* memoria;
	mem_virtual_init(&memoria, 4, 20);
	/*for(int i=0; i<30; i++)*/
	mem_virtual_insere(memoria, 1024, 3);
	mem_virtual_insere(memoria, 1025, 8);
	mem_virtual_insere(memoria, 2048, 38);
	mem_virtual_insere(memoria, 2500, 33);
	mem_virtual_insere(memoria, 3192, 48);
	mem_virtual_insere(memoria, 3200, 38);
	mem_virtual_insere(memoria, 4096, 387);
	mem_virtual_insere(memoria, 4204, 487);
	mem_virtual_acessa(memoria, 1024, &dado);
	mem_virtual_imprime(memoria);
	while(1){
		printf("Endereco e dado: ");
		scanf("%d %d", &endereco, &dado);
		mem_virtual_insere(memoria, endereco, dado);
		/*system("clear");*/
		mem_virtual_imprime(memoria);
	}
	/*mem_virtual_insere(memoria, 8192, 387);*/
	/*mem_virtual_insere(memoria, 8222, 487);*/
	/*mem_virtual_insere(memoria, 10240, 57);*/
	/*mem_virtual_insere(memoria, 10241, 11);*/
	/*mem_virtual_insere(memoria, 10245, 22);*/
	/*mem_virtual_insere(memoria, 12768, 78);*/
	/*mem_virtual_insere(memoria, 14345, 34);*/
	/*mem_virtual_insere(memoria, 16345, 55);*/
	/*mem_virtual_acessa(memoria, 10245, &dado);*/
	/*mem_virtual_acessa(memoria, 1025, &dado);*/
	return 0;
}
