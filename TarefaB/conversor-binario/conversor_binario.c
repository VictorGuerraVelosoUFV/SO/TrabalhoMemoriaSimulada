#include <stdio.h>
#include "conversor_binario.h"

bool* decimal_binario(const unsigned decimal, unsigned bitMaisEsquerda, unsigned bitMaisDireita){
	int tamanhoCutBin = (bitMaisEsquerda-bitMaisDireita+1);
	if(bitMaisEsquerda > 31 || bitMaisDireita > 31) return NULL;
	if(bitMaisEsquerda <= bitMaisDireita) return NULL;

	int valorInicial = decimal;
	bool* bin = (bool*) malloc(32*sizeof(bool));
	bool* cutBin = (bool*) malloc(tamanhoCutBin*sizeof(bool));
	int resto;

	for(int i=0; i<32; i++){
		resto = valorInicial%2;
		bin[i] = resto;
		valorInicial = valorInicial/2;
	}

	for(int i=tamanhoCutBin-1, j=bitMaisEsquerda; i>=0; i--, j--){
		cutBin[i] = bin[j];
	}
	
	free(bin);
	return cutBin;
}

int binario_decimal(const bool binario[], const unsigned size){
	int decimal=0;
	for(int i=0; i<size; i++){
		decimal += binario[i] * pow(2, i);
	}
	return decimal;
}

void binario_imprime(const bool binario[], const unsigned size){
	for(int i=size-1; i>=0; i--){
		printf("%d", binario[i]);
	}
}
