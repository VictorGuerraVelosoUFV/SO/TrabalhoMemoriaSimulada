#ifndef CONVERSOR_BINARIO_H
#define CONVERSOR_BINARIO_H
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>

bool* decimal_binario(const unsigned decimal, unsigned bitMaisEsquerda, unsigned bitMaisDireita);
int binario_decimal(const bool binario[], const unsigned size);
void binario_imprime(const bool binario[], const unsigned size);

#endif
