#ifndef GERADOR_H
#define GERADOR_H
#include <stdlib.h>
#include <time.h>
#include "../utils/utils.h"

int rand_int(int upperbound, int lowerbound);
int *rand_int_array(int upperbound, int lowerbound, int size);
#endif
