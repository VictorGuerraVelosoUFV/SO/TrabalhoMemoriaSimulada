#include "gerador.h"

int rand_int(int upperbound, int lowerbound){
	return upperbound - rand()%(upperbound-lowerbound+1);
}
int *rand_int_array(int upperbound, int lowerbound, int size){
    int *array = MALLOC(int,size);
    for(int i = 0; i < size; i++){
        array[i] = rand_int(upperbound,lowerbound);
    }
    return array;
}
