#include "mem_fisica.h"

int mem_fisica_init(MemoriaFisica** mem, unsigned size){
	(*mem) = (MemoriaFisica*) malloc(sizeof(MemoriaFisica));
	if(*mem == NULL) return -1;

	(*mem)->array = (Pagina**) malloc(size*sizeof(Pagina*));
	if((*mem)->array == NULL) return -1;

	(*mem)->size = size;

	for(int i=0; i<size; i++){
		(*mem)->array[i] = NULL;
	}

	return 0;
}

int mem_fisica_moldura_livre(MemoriaFisica* mem){
	for(int i=0; i<mem->size; i++){
		if(mem->array[i] == NULL)
			return i;
	}
	return -1;
}

int mem_fisica_aloca(MemoriaFisica* mem, int endereco, int deslocamento, int dado){
	if(mem->array[endereco] != NULL){
		pagina_set(mem->array[endereco], deslocamento, dado);
		return 0;
	}else{
		pagina_init(&mem->array[endereco]);
		pagina_set(mem->array[endereco], deslocamento, dado);
		return 0;
	}
}

Pagina* mem_fisica_remove(MemoriaFisica* mem, int endereco){
	if(endereco > mem->size) return NULL;
	Pagina* paginaRemovida = mem->array[endereco];
	mem->array[endereco] = NULL;
	return paginaRemovida;
}

int mem_fisica_acessa(MemoriaFisica* mem, int endereco, int deslocamento, int* dado){
	if(mem->array[endereco] == NULL) return -1;
	*dado = pagina_get(mem->array[endereco], deslocamento);
	return 0;
}

void mem_fisica_imprime(MemoriaFisica* mem){
	for(int i=0; i<mem->size; i++){
		if(mem->array[i] == NULL){
			printf("Pagina %d = NULL\n", i);
			continue;
		}
		printf("Pagina: %d\n", i); 
		for(int j=0; j<PAGESIZE; j++){
			if(!pagina_endereco_livre(mem->array[i], j)){
				printf("\t%d:%d\n", j, pagina_get(mem->array[i], j));
			}
		}
	}
}
