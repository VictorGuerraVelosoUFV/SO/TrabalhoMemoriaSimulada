#ifndef MEM_FISICA_H
#define MEM_FISICA_H
#include <stdio.h>
#include <stdlib.h>
#include "../pagina.h"

typedef struct{
	Pagina** array;
	unsigned size; //Size é o M
}MemoriaFisica;

/*mem_fisica_aloca retorna 0 no sucesso e -1 na falha*/
/*mem_fisica_acessa retorna o dado acessado no endereco e deslocamento passados como parametro*/
int mem_fisica_init(MemoriaFisica** mem, unsigned size);
int mem_fisica_moldura_livre(MemoriaFisica* mem); //Retorna o endereco de uma moldura livre ou -1 se a memoria estiver cheia
int mem_fisica_aloca(MemoriaFisica* mem, int endereco, int deslocamento, int dado);
Pagina* mem_fisica_remove(MemoriaFisica* mem, int endereco);
int mem_fisica_acessa(MemoriaFisica* mem, int endereco, int deslocamento, int* dado);
void mem_fisica_imprime(MemoriaFisica* mem);

#endif
