#ifndef PAGINA_H
#define PAGINA_H
#define PAGESIZE 1024
#include <stdbool.h>
typedef struct{
	unsigned addr[PAGESIZE];
	bool ehVazia[PAGESIZE];
}Pagina;

int pagina_init(Pagina** pagina);
void pagina_set(Pagina* const pagina, const int index, const int valor);
int pagina_get(const Pagina* const pagina, const int index);
bool pagina_endereco_livre(const Pagina* const pagina, const int index);

#endif
