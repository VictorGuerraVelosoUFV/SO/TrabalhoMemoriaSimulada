#include <stdlib.h>
#include <stdbool.h>
#include "pagina.h"

int pagina_init(Pagina** pagina){
	(*pagina) = (Pagina*) malloc(sizeof(Pagina));
	if(*pagina == NULL){ 
		return -1;
	}else{
		for(int i=0; i<PAGESIZE; i++){
			(*pagina)->ehVazia[i] = true;
			(*pagina)->addr[i] = 0;
		}
		return 0;
	}
}

void pagina_set(Pagina* const pagina, const int index, const int valor){
	pagina->addr[index] = valor;
	pagina->ehVazia[index] = false;
}

int pagina_get(const Pagina* const pagina, const int index){
	return pagina->addr[index];
}

bool pagina_endereco_livre(const Pagina* const pagina, const int index){
	return pagina->ehVazia[index];
}
