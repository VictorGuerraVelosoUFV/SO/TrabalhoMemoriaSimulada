#ifndef TABELA_PAGINAS_H
#define TABELA_PAGINAS_H
#include <stdbool.h>
#include "../../conversor-binario/conversor_binario.h"

#define ENDERECOPAGINA 19

typedef struct{
	bool enderecoPagina[ENDERECOPAGINA];
	bool bitPresente;
	bool modificada;
	bool referenciada;
}Entrada;

typedef struct{
	Entrada* entradas;
	unsigned tamanho;
}TabelaPaginas;

int tabela_paginas_init(TabelaPaginas** tabela, const unsigned P);
bool tabela_paginas_pagina_inicializada(TabelaPaginas* tabela, const unsigned index);
bool* tabela_paginas_acessa(const TabelaPaginas* const tabela, const unsigned index);
bool tabela_paginas_insere(TabelaPaginas* const tabela, const unsigned index, const bool* const enderecoPagina, const bool bitPresente);
bool* tabela_paginas_remove(TabelaPaginas* const tabela, const unsigned index);
bool tabela_paginas_presente(const TabelaPaginas* const tabela, const unsigned index);
bool tabela_paginas_referenciada(const TabelaPaginas* const tabela, const int index);
void tabela_paginas_limpa_referencias(TabelaPaginas* const tabela);
void tabela_paginas_imprime(const TabelaPaginas* const tabela);

#endif
