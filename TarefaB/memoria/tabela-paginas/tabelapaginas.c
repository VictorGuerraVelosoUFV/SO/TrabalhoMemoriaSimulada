#include <stdio.h>
#include "tabelapaginas.h"

int tabela_paginas_init(TabelaPaginas** tabela, const unsigned P){
	(*tabela) = (TabelaPaginas*) malloc(sizeof(TabelaPaginas));
	if(*tabela == NULL) return -1;
	(*tabela)->entradas = (Entrada*) malloc(P*sizeof(Entrada));
	if((*tabela)->entradas == NULL) return -1;
	(*tabela)->tamanho = P;
	for(int i=0; i<P; i++){
		for(int j=0; j<ENDERECOPAGINA; j++){
			(*tabela)->entradas[i].enderecoPagina[j] = 0;
		}
		(*tabela)->entradas[i].bitPresente = false;
		(*tabela)->entradas[i].modificada = false;
		(*tabela)->entradas[i].referenciada = false;
	}
	return 0;
}

bool tabela_paginas_pagina_inicializada(TabelaPaginas* tabela, const unsigned index){
	// 000..0 0 -> memoria nao inicializada
	// xxx..x 1 -> memoria virtual inicializada na memoria principal
	// xxx..x 0 -> memoria virtual inicializada na memoria secundaria

	for(int i=0; i<ENDERECOPAGINA; i++){
		if(tabela->entradas[index].enderecoPagina[i] == 1)
			return true;
	}
	return tabela->entradas[index].bitPresente;
}

bool* tabela_paginas_acessa(const TabelaPaginas* const tabela, const unsigned index){
	tabela->entradas[index].referenciada = true;
	return tabela->entradas[index].enderecoPagina;
}

bool tabela_paginas_insere(TabelaPaginas* const tabela, const unsigned index, const bool* const enderecoPagina, const bool bitPresente){
	if(index > tabela->tamanho) return false;
	tabela->entradas[index].bitPresente = bitPresente;
	tabela->entradas[index].modificada = false;
	tabela->entradas[index].referenciada = true;
	for(int i=0; i<ENDERECOPAGINA; i++){
		tabela->entradas[index].enderecoPagina[i] = enderecoPagina[i];
	}
}

bool* tabela_paginas_remove(TabelaPaginas* const tabela, const unsigned index){
	bool* enderecoPaginaRemovida = (bool*) malloc(ENDERECOPAGINA*sizeof(bool));
	if(index > tabela->tamanho) return NULL;
	tabela->entradas[index].bitPresente = false;
	tabela->entradas[index].modificada = false;
	tabela->entradas[index].referenciada = false;

	for(int i=0; i<ENDERECOPAGINA; i++){
		enderecoPaginaRemovida[i] = tabela->entradas[index].enderecoPagina[i];
		tabela->entradas[index].enderecoPagina[i] = 0;
	}

	return enderecoPaginaRemovida;

}

bool tabela_paginas_presente(const TabelaPaginas* const tabela, const unsigned index){
	return tabela->entradas[index].bitPresente;
}

bool tabela_paginas_referenciada(const TabelaPaginas* const tabela, const int index){
	return tabela->entradas[index].referenciada;
}

void tabela_paginas_limpa_referencias(TabelaPaginas* const tabela){
	for(int i=0; i<tabela->tamanho; i++)
		tabela->entradas[i].referenciada = false;
}

void tabela_paginas_imprime(const TabelaPaginas* const tabela){
	printf("I:\t|\tBP\t|\tBR\t| ------Page addr---- |\n");
	for(int i=0; i<tabela->tamanho; i++){
		printf("%d:\t|\t%d\t|\t%d\t| ", i, tabela->entradas[i].bitPresente, tabela->entradas[i].referenciada);
		binario_imprime(tabela->entradas[i].enderecoPagina, ENDERECOPAGINA);
		printf(" |\n");
	}
}
