#ifndef MEM_VIRTUAL_H
#define MEM_VIRTUAL_H
#include "../tabela-paginas/tabelapaginas.h"
#include "../fisica/mem_fisica.h"
#include "../arquivos-binarios/arquivos-binarios.h"
#include "../../linked-list/linkedlist.h"
#define DESLOCAMENTO 10 //2^10 = 1024 posições dentro da página a serem endereçadas

typedef struct{
	MemoriaFisica* memoriaFisica;
	ArquivosBinarios* arquivosBinarios;
	TabelaPaginas* tabelaPaginas;
	LList* ordemInsercao;
}MemoriaVirtual;

int mem_virtual_init(MemoriaVirtual** memoria, const unsigned M, const unsigned P);
int mem_virtual_insere(MemoriaVirtual* const memoria, const unsigned endereco, const int dado);
int mem_virtual_substitui_fifo(MemoriaVirtual* const memoria, const int cabecalhoEndereco, const int caldaEndereco, const int dado);
int mem_virtual_substitui_segunda_chance(MemoriaVirtual* const memoria, const int cabecalhoEndereco, const int caldaEndereco, const int dado);
int mem_virtual_acessa(const MemoriaVirtual* const memoria, const unsigned endereco, int* dado);
void mem_virtual_imprime(const MemoriaVirtual* const memoria);
#endif
