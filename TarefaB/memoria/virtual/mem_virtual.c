#include <stdbool.h>
#include <stdio.h>
#include "mem_virtual.h"

int mem_virtual_init(MemoriaVirtual** memoria, const unsigned M, const unsigned P){
	(*memoria) = (MemoriaVirtual*) malloc(sizeof(MemoriaVirtual));
	if(*memoria == NULL) return -1;
	if(mem_fisica_init(&(*memoria)->memoriaFisica, M) == -1) return -1;
	if(arquivos_binarios_init(&(*memoria)->arquivosBinarios, P, 4096) == 0) return -1;
	if(tabela_paginas_init(&(*memoria)->tabelaPaginas, P) == -1) return -1;
	if(ll_initialize(&(*memoria)->ordemInsercao) == -1) return -1;
	return 0;
}

// 000..0 0 -> memoria nao inicializada
// xxx..x 1 -> memoria virtual inicializada na memoria principal
// xxx..x 0 -> memoria virtual inicializada na memoria secundaria
int mem_virtual_insere(MemoriaVirtual* const memoria, const unsigned endereco, const int dado){
	unsigned cabecalhoEndereco;
	unsigned caldaEndereco;


	//Obter o indice da tabela de paginas (cabecalho)
	cabecalhoEndereco = binario_decimal(decimal_binario(endereco, 31, 10), 22);
	//Obter o deslocamento (calda)
	caldaEndereco = binario_decimal(decimal_binario(endereco, 9, 0), 10);

	//Se a tabela já apontar para uma página na memoria principal
	if(tabela_paginas_pagina_inicializada(memoria->tabelaPaginas, cabecalhoEndereco)){
		const int enderecoPaginaFisicaExistente = binario_decimal(tabela_paginas_acessa(memoria->tabelaPaginas, cabecalhoEndereco), 19);
		//Limpa referencias anteriores à tabela de páginas
		tabela_paginas_limpa_referencias(memoria->tabelaPaginas);
		mem_fisica_aloca(memoria->memoriaFisica, enderecoPaginaFisicaExistente, caldaEndereco, dado);
		return 0;
	}

	//Daqui pra frente, uma moldura livre na memoria fisica é procurada e adicionada no endereco vazio da tabela de paginas
	const int molduraLivre = mem_fisica_moldura_livre(memoria->memoriaFisica);

	//Se não houver espaço, tratar com um algoritmo (FIFO ou Segunda Chance)
	if(molduraLivre == -1){
		return mem_virtual_substitui_segunda_chance(memoria, cabecalhoEndereco, caldaEndereco, dado);
	}
		
	bool* cabecalhoEnderecoBin = decimal_binario(endereco, 31, 10);
	bool* molduraLivreBin = decimal_binario(molduraLivre, 18, 0);
	//Insere-se o endereco (relativo a tabela de paginas) da pagina na lista encadeada
	ll_insert(memoria->ordemInsercao, (void**) &cabecalhoEnderecoBin);
	//Em seguida, a entrada da TB referente ao cabeçalho é atualizada com o local da moldura livre
	tabela_paginas_insere(memoria->tabelaPaginas, cabecalhoEndereco, molduraLivreBin, true);
	//Logo após, é alocado o dado na página física no deslocamento encontrado
	mem_fisica_aloca(memoria->memoriaFisica, molduraLivre, caldaEndereco, dado);

}

int mem_virtual_substitui_fifo(MemoriaVirtual* const memoria, const int cabecalhoEndereco, const int caldaEndereco, const int dado){
	/* Movendo a pagina antiga para o disco */
	Pagina* paginaRemovida = NULL;
	//Remove da LL o endereco da tabela de paginas da pagina a ser levada ao arquivo
	const bool* enderecoTabelaBin = ll_remove_head(memoria->ordemInsercao);
	const bool* enderecoMemoriaBin = tabela_paginas_acessa(memoria->tabelaPaginas, binario_decimal(enderecoTabelaBin, 19));
	const int enderecoTabela = binario_decimal(enderecoTabelaBin, 19);
	const int enderecoMemoria = binario_decimal(enderecoMemoriaBin, 19);
	//Remove-se com este endereco, a pagina da memoria principal
	paginaRemovida = mem_fisica_remove(memoria->memoriaFisica, enderecoMemoria);
	//Obtem-se um endereco de arquivo para alocar a pagina
	const int arquivoLivre = arquivos_binarios_arquivo_livre(memoria->arquivosBinarios);
	if(arquivoLivre == -1) return -1;
	//Atualiza a tabela de paginas com este endereco de arquivo
	tabela_paginas_insere(memoria->tabelaPaginas, enderecoTabela, decimal_binario(arquivoLivre, 18, 0), false);
	//Adiciona essa pagina a este arquivo
	for(int i=0; i<PAGESIZE; i++)
		if(!pagina_endereco_livre(paginaRemovida, i))
			arquivos_binarios_aloca(memoria->arquivosBinarios, arquivoLivre, i, pagina_get(paginaRemovida, i));
	//Libera a página da memória
	free(paginaRemovida);

	/* Adicionando nova pagina na memoria */
	const bool* enderecoTabelaNovoBin = decimal_binario(cabecalhoEndereco, 18, 0);
	const int enderecoTabelaNovo = binario_decimal(enderecoTabelaNovoBin, 19);
	const int enderecoMemoriaNovo = enderecoMemoria;
	const bool* enderecoMemoriaNovoBin = decimal_binario(enderecoMemoria, 18, 0);
	//Usa-se o endereco de memoria da pagina antiga para alocar a nova
	mem_fisica_aloca(memoria->memoriaFisica, enderecoMemoriaNovo, caldaEndereco, dado);
	//Coloca o endereco da nova entrada na lista encadeada
	ll_insert(memoria->ordemInsercao, (void**) &enderecoTabelaNovoBin);
	//Atualiza o endereco na tabela de paginas com o novo endereco de arquivo
	tabela_paginas_insere(memoria->tabelaPaginas, enderecoTabelaNovo, enderecoMemoriaNovoBin, false);

}

int mem_virtual_substitui_segunda_chance(MemoriaVirtual* const memoria, const int cabecalhoEndereco, const int caldaEndereco, const int dado){
	/* Movendo a pagina antiga para o disco */
	Pagina* paginaRemovida = NULL;
	
	//Remove da LL o endereco da tabela de paginas da pagina a ser levada ao arquivo
	bool* enderecoTabelaBin = ll_remove_head(memoria->ordemInsercao);
	int enderecoTabela = binario_decimal(enderecoTabelaBin, 19);
	const int enderecoTabelaInicial = enderecoTabela;
	while(tabela_paginas_referenciada(memoria->tabelaPaginas, enderecoTabela)){
		ll_insert(memoria->ordemInsercao, (void**) &enderecoTabelaBin);
		enderecoTabelaBin = ll_remove_head(memoria->ordemInsercao);
		enderecoTabela = binario_decimal(enderecoTabelaBin, 19);
		if(enderecoTabelaInicial == enderecoTabela) break;
	}
	//Limpa referencias anteriores à tabela de páginas
	tabela_paginas_limpa_referencias(memoria->tabelaPaginas);

	const bool* enderecoMemoriaBin = tabela_paginas_acessa(memoria->tabelaPaginas, binario_decimal(enderecoTabelaBin, 19));
	const int enderecoMemoria = binario_decimal(enderecoMemoriaBin, 19);
	//Remove-se com este endereco, a pagina da memoria principal
	paginaRemovida = mem_fisica_remove(memoria->memoriaFisica, enderecoMemoria);
	//Obtem-se um endereco de arquivo para alocar a pagina
	const int arquivoLivre = arquivos_binarios_arquivo_livre(memoria->arquivosBinarios);
	if(arquivoLivre == -1) return -1;
	//Atualiza a tabela de paginas com este endereco de arquivo
	tabela_paginas_insere(memoria->tabelaPaginas, enderecoTabela, decimal_binario(arquivoLivre, 18, 0), false);
	//Adiciona essa pagina a este arquivo
	for(int i=0; i<PAGESIZE; i++)
		if(!pagina_endereco_livre(paginaRemovida, i))
			arquivos_binarios_aloca(memoria->arquivosBinarios, arquivoLivre, i, pagina_get(paginaRemovida, i));
	//Libera a página da memória
	free(paginaRemovida);

	/* Adicionando nova pagina na memoria */
	const bool* enderecoTabelaNovoBin = decimal_binario(cabecalhoEndereco, 18, 0);
	const int enderecoTabelaNovo = binario_decimal(enderecoTabelaNovoBin, 19);
	const int enderecoMemoriaNovo = enderecoMemoria;
	const bool* enderecoMemoriaNovoBin = decimal_binario(enderecoMemoria, 18, 0);
	//Usa-se o endereco de memoria da pagina antiga para alocar a nova
	mem_fisica_aloca(memoria->memoriaFisica, enderecoMemoriaNovo, caldaEndereco, dado);
	//Coloca o endereco da nova entrada na lista encadeada
	ll_insert(memoria->ordemInsercao, (void**) &enderecoTabelaNovoBin);
	//Atualiza o endereco na tabela de paginas com o novo endereco de arquivo
	tabela_paginas_insere(memoria->tabelaPaginas, enderecoTabelaNovo, enderecoMemoriaNovoBin, false);

}

int mem_virtual_acessa(const MemoriaVirtual* const memoria, const unsigned endereco, int* dado){
	int enderecoTabelaPaginas;
	int cabecalhoEnderecoFisico;
	int caldaEnderecoFisico;
	bool* enderecoFisicoEsquerdaBin;
	bool* enderecoFisicoDireitaBin;
	bool* temp = NULL;

	//Limpa referencias anteriores à tabela de páginas
	tabela_paginas_limpa_referencias(memoria->tabelaPaginas);

	//Obter o endereco na tabela de paginas
	temp = decimal_binario(endereco, 31, 10);
	enderecoTabelaPaginas = binario_decimal(temp, 22);
	free(temp);

	//Acessar, no endereco encontrado, os valores binarios do cabeçalho do endereco fisico
	enderecoFisicoEsquerdaBin = tabela_paginas_acessa(memoria->tabelaPaginas, enderecoTabelaPaginas);
	cabecalhoEnderecoFisico = binario_decimal(enderecoFisicoEsquerdaBin, ENDERECOPAGINA);

	//Obter o deslocamento
	enderecoFisicoDireitaBin = decimal_binario(endereco, 9, 0);
	caldaEnderecoFisico = binario_decimal(enderecoFisicoDireitaBin, 10);
	free(enderecoFisicoDireitaBin);

	//Acessa o endereco fisico com os endereço e deslocamento obtido
	return mem_fisica_acessa(memoria->memoriaFisica, cabecalhoEnderecoFisico, caldaEnderecoFisico, dado);
}

void mem_virtual_imprime(const MemoriaVirtual* const memoria){
	printf("\nTabela de Páginas:\n");
	tabela_paginas_imprime(memoria->tabelaPaginas);
	printf("\nMemoria Fisica:\n");
	mem_fisica_imprime(memoria->memoriaFisica);
	printf("\nPaginas Virtuais:\n");
	arquivos_binarios_imprime(memoria->arquivosBinarios);
	printf("\nLista Encadeada:\n");
	ll_print(memoria->ordemInsercao);
}
