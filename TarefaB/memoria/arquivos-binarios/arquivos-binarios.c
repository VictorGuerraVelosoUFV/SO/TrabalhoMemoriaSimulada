#include <stdio.h>
#include "arquivos-binarios.h"
//Inicializa disco e seus arquivos com 1024 valores aleatorios
bool arquivos_binarios_init(ArquivosBinarios** mem, unsigned P, unsigned tamanhoBytesArquivo){
	char filename[16];
	const int inteiros = tamanhoBytesArquivo/4;
	int* randomNumbers = rand_int_array(20, 10, inteiros); //gera 1024 inteiros aleatorios

	(*mem) = (ArquivosBinarios*) malloc(sizeof(ArquivosBinarios));
	(*mem)->array = (FileManager**) malloc(P*sizeof(FileManager*));
	(*mem)->size = P;
	(*mem)->tamanhoBytesArquivos = tamanhoBytesArquivo;

	(*mem)->bitValido = (bool**) malloc(P*sizeof(bool*)); //Para cada arquivo
	for(int i=0; i<P; i++){ (*mem)->bitValido[i] = (bool*) malloc(inteiros*sizeof(bool)); } //Inicia-se um vetor booleano
	for(int i=0; i<P; i++){  //Ele é iniciado todo falso
		for(int j=0; j<inteiros; j++){
			(*mem)->bitValido[i][j] = false;
		}
	}

	for(int i = 0; i < P; i++){
		sprintf(filename, "%d.page", i);
		if(fileManagerOpenFile(&(*mem)->array[i],filename,"wb")){
			for(int j=0; j<inteiros; j++){ //escreve 1024 inteiros aleatorios
				fwrite(&randomNumbers[j], sizeof(int), 1, (*mem)->array[i]->file);
			}
			free(randomNumbers);
			randomNumbers = rand_int_array(20, 10, inteiros);
			fileManagerClose((*mem)->array[i]);
		}else{
			fprintf(stderr,"Não é possível criar arquivo %s!",filename);
			free(randomNumbers);
			free((*mem)->array);
			free(*mem);
			return false;
		}
	}
	return true;
}

//Sobrescreve valor na posicao apos o deslocamento
bool arquivos_binarios_aloca(ArquivosBinarios* mem, int endereco, int deslocamento, int dado){
	char filename[16];
	sprintf(filename, "%d.page", endereco);
	if(endereco > mem->size) return false;
	if(!fileManagerOpenFile(&mem->array[endereco], filename, "r+b")) return false;
	if(fseek(mem->array[endereco]->file, deslocamento*sizeof(int), SEEK_SET) == -1) return false;
	fwrite(&dado, sizeof(int), 1, mem->array[endereco]->file);
	mem->bitValido[endereco][deslocamento] = true;
	fileManagerClose(mem->array[endereco]);
	return true;
}

int arquivos_binarios_arquivo_livre(ArquivosBinarios* mem){
	for(int i=1; i<mem->size; i++){
		for(int j=0; j<mem->tamanhoBytesArquivos/4; j++){
			if(mem->bitValido[i][j]) break;
			if(j == mem->tamanhoBytesArquivos/4-1) return i;
		}
	}
}

//Le valor na posicao apos o deslocamento
bool arquivos_binarios_acessa(ArquivosBinarios* mem, int endereco, int deslocamento, int* dado){
	char filename[16];
	sprintf(filename, "%d.page", endereco);
	if(endereco > mem->size) return false;
	if(!fileManagerOpenFile(&mem->array[endereco], filename, "rb")) return false;
	if(fseek(mem->array[endereco]->file, deslocamento*sizeof(int), SEEK_SET) == -1) return false;
	fread(dado, sizeof(int), 1, mem->array[endereco]->file);
	fileManagerClose(mem->array[endereco]);
	return true;
}

bool arquivos_binarios_valido(ArquivosBinarios* mem, int endereco, int deslocamento){
	return (mem->bitValido[endereco][deslocamento]);
}

//Imprime conteudo de todos os arquivos
void arquivos_binarios_imprime(ArquivosBinarios* mem){
	char filename[16];
	const int inteiros = mem->tamanhoBytesArquivos/4;
	int dado;
	for (int i = 0; i < mem->size; i++){
		sprintf(filename, "%d.page", i);
		for(int j=0; j<inteiros; j++){
			if(!arquivos_binarios_valido(mem, i, j)) continue;
			if(j==0) printf("Arquivo %s\n", filename);
			arquivos_binarios_acessa(mem, i, j, &dado);
			printf("-----> %d:%d\n", j, dado);
			if(j==inteiros-1) printf("\n\n");
		}
	}
}
