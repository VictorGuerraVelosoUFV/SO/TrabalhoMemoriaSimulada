#ifndef ARQUIVOS_BINARIOS_H
#define ARQUIVOS_BINARIOS_H
#include <stdio.h>
#include <stdlib.h>
#include "../../utils/utils.h"
#include "../../c-library-filemanagement/src/filemanager.h"
#include "../../gerador-randomico/gerador.h"
#include <limits.h>
#include <stdbool.h>
#define INTEGERFIELDS 11

typedef struct{
	FileManager** array;
	bool** bitValido;
	unsigned tamanhoBytesArquivos;
	unsigned size; //Size é o M
}ArquivosBinarios;

bool arquivos_binarios_init(ArquivosBinarios** mem, unsigned P, unsigned tamanhoBytesArquivo);
bool arquivos_binarios_aloca(ArquivosBinarios* mem, int endereco, int deslocamento, int dado);
int arquivos_binarios_arquivo_livre(ArquivosBinarios* mem);
bool arquivos_binarios_acessa(ArquivosBinarios* mem, int endereco, int deslocamento, int *dado);
bool arquivos_binarios_valido(ArquivosBinarios* mem, int endereco, int deslocamento);
void arquivos_binarios_imprime(ArquivosBinarios* mem);

#endif
