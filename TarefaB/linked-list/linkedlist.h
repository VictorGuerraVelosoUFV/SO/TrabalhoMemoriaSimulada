#ifndef LINKED_LIST
#define LINKED_LIST
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../utils/utils.h"
#include "../conversor-binario/conversor_binario.h"

typedef struct LLNode{
	void* data;
	struct LLNode* next;
	struct LLNode* previous;
}LLNode;

typedef struct{
	LLNode* first;
	LLNode* last;
}LList;

int ll_initialize(LList** list);
int ll_insert(LList* list, void** data);
bool* ll_remove_head(LList* list);
//LLNode* ll_remove(LList* list, void* data);
LLNode* ll_remove_aux(LList* list, LLNode** node);
void ll_print(LList* list);
#endif

