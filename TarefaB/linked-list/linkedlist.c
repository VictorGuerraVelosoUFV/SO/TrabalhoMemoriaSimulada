#include "linkedlist.h"

int ll_initialize(LList** list){
	if ( ((*list) = MALLOC(LList,1) ) == NULL ) return -1;
	if ( ((*list)->first = MALLOC(LLNode,1) ) == NULL ) return -1;
	(*list)->last = (*list)->first;
	(*list)->first->next = NULL;
	(*list)->first->previous = NULL;
	(*list)->first->data = NULL;
	return 0;
}

int ll_insert(LList* list, void** data){
	LLNode* newNode;
	if ( (newNode = MALLOC(LLNode,1) ) == NULL ) return -1;
	newNode->data = *data;
	newNode->next = list->last->next;
	list->last->next = newNode;
	newNode->previous = list->last;
	list->last = newNode;

	return 0;
}

bool* ll_remove_head(LList* list){
	if(list->first == list->last) return NULL;

	LLNode* current = list->first->next;
	LLNode* removed = NULL;

	return (bool*) ll_remove_aux(list, &current)->data;
}

LLNode* ll_remove_aux(LList* list, LLNode** node){
	if(*node == list->last){
		list->last = (*node)->previous;
		(*node)->previous->next = NULL;
		return *node;
	}
	(*node)->previous->next = (*node)->next;
	(*node)->next->previous = (*node)->previous;
	return *node;
}

void ll_print(LList* list){
	LLNode* aux = list->first->next;

	if(aux == NULL) return;

	while(aux != NULL){
		binario_imprime(aux->data, 19);
		printf("\n");
		aux = aux->next;
	}
}
