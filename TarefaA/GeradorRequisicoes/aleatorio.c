/*
 *
 * Copyright (c) 2018, Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the UFV nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "aleatorio.h"

#define GEN_OFFSET 20

int gerar(int sz, int range, int **pids, int **pszs){
    int in = 1, im = 0, rn, rm, i = 0, psz, j;
    if((*pids) == NULL) (*pids) = MALLOC(int, sz);
    if((*pszs) == NULL) (*pszs) = MALLOC(int, sz);
    while(i != sz){
        rn = range - in;
        rm = sz - im;
        if (rand() % rn < rm){
            j = 0;
            do{
                psz = ceil(powf(2, (rand()%7) + 1)); //Tamanho do processo [2:128]
                j++;
            }while(j < 5 && psz + i < sz); //Nao pode ultrapassar o tamanho da memoria
            if (psz + i >= sz) psz = 2;
            i += psz;
            (*pids)[im] = rand() % in + 1;
            (*pszs)[im++] = psz;
            if(i >= sz || in >= range)
                (*pszs)[im-1] += sz-i;
            printf("PID: %d SZ: %d\n i: %d\n",(*pids)[im-1],(*pszs)[im-1],i);
        }
        in += GEN_OFFSET;
    }
    REALLOC(int,im,(*pids));
    REALLOC(int,im,(*pszs));
    return im;
}
/*int main(){
    int *pids = NULL, *pszs = NULL;
    srand(time(NULL));
    int res = gerar(256, 1000, &pids, &pszs);
    int soma = 0;
    printf("\n\n\n************************************************************************************\n\n\n\n");
    for(int i = 0; i < res; i++){
        soma += pszs[i];
        printf("PID: %d SZ: %d\n i: %d\n",pids[i],pszs[i],soma);
    }
    return 0;
}*/
