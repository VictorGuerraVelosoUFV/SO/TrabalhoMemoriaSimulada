/*
 *
 * Copyright (c) 2018, Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the UFV nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "arquivo.h"

#define N 1000

int gerar(int sz, int **pids, int **pszs){
    int i = 0, j = 0;
    FILE *file = fopen("input.dat","r");
    if((*pids) == NULL) (*pids) = MALLOC(int, sz);
    if((*pszs) == NULL) (*pszs) = MALLOC(int, sz);
    while(!feof(file) && j != sz){
        fscanf(file,"%d %d",(*pids)+i,(*pszs)+i);
        j += (*pszs)[i];
        printf("PID: %d SZ: %d\n i: %d\n",(*pids)[i],(*pszs)[i],j);
        if(j > sz) j -= (*pszs)[i];
        else i++;
    }
    (*pszs)[i-1] += sz-j;
    REALLOC(int,i,(*pids));
    REALLOC(int,i,(*pszs));
    return i;
}
/*int main(){
    int *pids = NULL, *pszs = NULL;
    srand(time(NULL));
    int res = gerar(256, &pids, &pszs);
    int soma = 0;
    printf("\n\n\n************************************************************************************\n\n\n\n");
    for(int i = 0; i < res; i++){
        soma += pszs[i];
        printf("PID: %d SZ: %d\n i: %d\n",pids[i],pszs[i],soma);
    }
    return 0;
}*/
