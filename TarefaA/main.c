/*
 * Copyright (c) 2018, Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the UFV nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "TecnicasAlocacao/Desempenho/desempenho.h"
#include "GeradorRequisicoes/aleatorio.h"
#include "TecnicasAlocacao/bestfit.h"
#include "TecnicasAlocacao/worstfit.h"
#include "TecnicasAlocacao/nextfit.h"
#include "TecnicasAlocacao/firstfit.h"

int main(int argc, char **argv) {
    srand(time(NULL));
    int res;
    int alocou;
    int id;
    int neg = 0;
    int *pids = NULL, *pszs = NULL;
    int ff_fragmento = 0;
    int bf_fragmento = 0;
    int wf_fragmento = 0;
    int nf_fragmento = 0;

    double ff_tempo = 0;
    double bf_tempo = 0;
    double wf_tempo = 0;
    double nf_tempo = 0;

    int ff_negado = 0;
    int bf_negado = 0;
    int wf_negado = 0;
    int nf_negado = 0;

    ff_Memoria *ff_mem;
    bf_Memoria *bf_mem;
    nf_Memoria *nf_mem;
    wf_Memoria *wf_mem;

    Desempenho ff_desempenho;
    Desempenho bf_desempenho;
    Desempenho nf_desempenho;
    Desempenho wf_desempenho;


    ff_inicializa_memoria(&ff_mem);
    bf_inicializa_memoria(&bf_mem);
    wf_inicializa_memoria(&wf_mem);
    nf_inicializa_memoria(&nf_mem);

    inicializa_desempenho(&ff_desempenho);
    inicializa_desempenho(&bf_desempenho);
    inicializa_desempenho(&nf_desempenho);
    inicializa_desempenho(&wf_desempenho);


    ff_negado= ff_aloca_memoria(ff_mem, 14578, 8,&ff_desempenho);

    if(ff_negado == -1){
        ff_desempenho.ff_negado++;
    }
    ff_imprime_memoria(ff_mem);
    ff_negado = ff_aloca_memoria(ff_mem, 9437, 8,&ff_desempenho);
    if(ff_negado == -1){
        ff_desempenho.ff_negado++;
    }
   ff_imprime_memoria(ff_mem);
   ff_negado = ff_aloca_memoria(ff_mem, 8576, 16,&ff_desempenho);
   if(ff_negado == -1){
        ff_desempenho.ff_negado++;
   }
    ff_imprime_memoria(ff_mem);
    ff_negado = ff_aloca_memoria(ff_mem, 876, 240,&ff_desempenho);
    if(ff_negado == -1){
        ff_desempenho.ff_negado++;
    }
    ff_imprime_memoria(ff_mem);
    ff_libera_memoria(ff_mem, 9437);
    ff_imprime_memoria(ff_mem);

    ff_negado = ff_aloca_memoria(ff_mem, 9437, 8,&ff_desempenho);
    if(ff_negado == -1){
         ff_desempenho.ff_negado++;
    }
    ff_imprime_memoria(ff_mem);

    printf("\nFF Negado %d\n",ff_desempenho.ff_negado);
    ff_negado = ff_percentual_requisicao_negada(&ff_desempenho);
    printf("\nFF Requisicao Negada %d\n",ff_negado);
    ff_fragmento = ff_numero_medio_fragmentos(&ff_desempenho);
    printf("\nFF Fragmento %d\n",ff_fragmento);
    ff_tempo = ff_tempo_medio_alocacao(&ff_desempenho);
    printf("\nFF Tempo %lf\n",ff_tempo);



    bf_negado= bf_aloca_memoria(bf_mem, 14578, 8,&bf_desempenho);
    if(bf_negado == -1){
        bf_desempenho.bf_negado++;
    }
    bf_imprime_memoria(bf_mem);
    bf_negado = bf_aloca_memoria(bf_mem, 9437, 8,&bf_desempenho);
    if(bf_negado == -1){
        bf_desempenho.bf_negado++;
    }
   bf_imprime_memoria(bf_mem);
   bf_negado = bf_aloca_memoria(bf_mem, 8576, 16,&bf_desempenho);
   if(bf_negado == -1){
       bf_desempenho.bf_negado++;
   }
    bf_imprime_memoria(bf_mem);
    bf_negado = bf_aloca_memoria(bf_mem, 876, 240,&bf_desempenho);
    if(bf_negado == -1){
        bf_desempenho.bf_negado++;
    }
    bf_imprime_memoria(bf_mem);
    bf_libera_memoria(bf_mem, 9437);
    bf_imprime_memoria(bf_mem);
    bf_negado = bf_aloca_memoria(bf_mem, 9437, 8,&bf_desempenho);
    if(bf_negado == -1){
        bf_desempenho.bf_negado++;
    }
   bf_imprime_memoria(bf_mem);

    printf("\nBF Negado %d\n",bf_desempenho.bf_negado);
    bf_negado = bf_percentual_requisicao_negada(&bf_desempenho);
    printf("\nBF Requisicao Negada %d\n",bf_negado);
    bf_fragmento = bf_numero_medio_fragmentos(&bf_desempenho);   
    printf("\nBF Fragmento %d\n",bf_fragmento);
    bf_tempo = bf_tempo_medio_alocacao(&bf_desempenho);
    printf("\nBF Tempo %lf\n",bf_tempo);



    nf_negado = nf_aloca_memoria(nf_mem, 14578, 8, &nf_desempenho);
    if (nf_negado == -1) {
        nf_desempenho.nf_negado++;
    }
    nf_imprime_memoria(nf_mem);
    nf_negado = nf_aloca_memoria(nf_mem, 9437, 8, &nf_desempenho);
    if (nf_negado == -1) {
        nf_desempenho.nf_negado++;
    }
    nf_imprime_memoria(nf_mem);
    nf_negado = nf_aloca_memoria(nf_mem, 8576, 16, &nf_desempenho);
    if (nf_negado == -1) {
        nf_desempenho.nf_negado++;
    }
    nf_imprime_memoria(nf_mem);
    nf_libera_memoria(nf_mem, 9437);
    nf_imprime_memoria(nf_mem);
    nf_negado = nf_aloca_memoria(nf_mem, 9437, 8, &nf_desempenho);
    if (nf_negado == -1) {
        nf_desempenho.nf_negado++;
    }
    nf_imprime_memoria(nf_mem);

    printf("\nNF Negado %d\n", nf_desempenho.nf_negado);
    nf_negado = nf_percentual_requisicao_negada(&nf_desempenho);
    printf("\nNF Requisicao Negada %d\n", nf_negado);
    nf_fragmento = nf_numero_medio_fragmentos(&nf_desempenho);
    printf("\nNF Fragmento %d\n", nf_fragmento);
    nf_tempo = nf_tempo_medio_alocacao(&nf_desempenho);
    printf("\nNF Tempo %lf\n", nf_tempo);


 wf_negado= wf_aloca_memoria(wf_mem, 14578, 8,&wf_desempenho);
    if(wf_negado == -1){
    	wf_desempenho.wf_negado++;
    }
    wf_imprime_memoria(wf_mem);
    wf_negado = wf_aloca_memoria(wf_mem, 9437, 8,&wf_desempenho);
    if(wf_negado == -1){
        wf_desempenho.wf_negado++;
    }
   wf_imprime_memoria(wf_mem);
   wf_negado = wf_aloca_memoria(wf_mem, 8576, 16,&wf_desempenho);
   if(wf_negado == -1){
        wf_desempenho.wf_negado++;
   }
    wf_imprime_memoria(wf_mem);
    wf_negado = wf_aloca_memoria(wf_mem, 876, 240,&wf_desempenho);
    if(wf_negado == -1){
        wf_desempenho.wf_negado++;
    }
    wf_imprime_memoria(wf_mem);
    wf_libera_memoria(wf_mem, 9437);
    wf_imprime_memoria(wf_mem);
    wf_negado = wf_aloca_memoria(wf_mem, 9437, 8,&wf_desempenho);
    if(wf_negado == -1){
        wf_desempenho.wf_negado++;
    }
    wf_imprime_memoria(wf_mem);

    printf("\nWF Negado %d\n",wf_desempenho.wf_negado);
    wf_negado = wf_percentual_requisicao_negada(&wf_desempenho);
    printf("\nWF Requisicao Negada %d\n",wf_negado);
    wf_fragmento = wf_numero_medio_fragmentos(&wf_desempenho);   
    printf("\nWF Fragmento %d\n",wf_fragmento);
    wf_tempo = wf_tempo_medio_alocacao(&wf_desempenho);
    printf("\nWF Tempo %lf\n",wf_tempo);


}
