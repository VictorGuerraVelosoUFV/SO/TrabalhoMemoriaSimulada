cmake_minimum_required(VERSION 3.10)
project(TarefaA)

option(GENBYFILE "Read input by file instead of random generation" OFF)
add_subdirectory(GeradorRequisicoes)
add_subdirectory(TecnicasAlocacao)
set(SRC_FILES main.c)
include_directories(${PROJECT_SOURCE_DIR})
add_executable(TarefaA ${SRC_FILES})
target_link_libraries(TarefaA TecnicasAlocacao GeradorRequisicoes)

install(TARGETS TarefaA RUNTIME DESTINATION bin)

MESSAGE(STATUS "Build Step: TarefaA")
MESSAGE(STATUS "Build type: " ${CMAKE_BUILD_TYPE})
MESSAGE(STATUS "Library Type: " ${LIB_TYPE})
MESSAGE(STATUS "Compiler flags:" ${CMAKE_CXX_COMPILE_FLAGS})
MESSAGE(STATUS "Compiler cxx debug flags:" ${CMAKE_CXX_FLAGS_DEBUG})
MESSAGE(STATUS "Compiler cxx release flags:" ${CMAKE_CXX_FLAGS_RELEASE})
MESSAGE(STATUS "Compiler cxx min size flags:" ${CMAKE_CXX_FLAGS_MINSIZEREL})
MESSAGE(STATUS "Compiler cxx flags:" ${CMAKE_CXX_FLAGS})
