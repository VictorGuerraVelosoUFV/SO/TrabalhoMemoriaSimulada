/*
 * Copyright (c) 2018, Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the UFV nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DESEMPENHO_H
#define DESEMPENHO_H
#include <time.h>

typedef struct desempenho{

    int bf_fragmento;
    int bf_contador;
    double bf_tempo;
    int bf_negado;

    int ff_fragmento;
    int ff_contador;
    double ff_tempo;
    int ff_negado;

    int wf_fragmento;
    int wf_contador;
    double wf_tempo;
    int wf_negado;

    int nf_fragmento;
    int nf_contador;
    double nf_tempo;
    int nf_negado;
}Desempenho;

int inicializa_desempenho(Desempenho *desempenho);
int bf_numero_medio_fragmentos(Desempenho *desempenho);
int wf_numero_medio_fragmentos(Desempenho *desempenho);
int nf_numero_medio_fragmentos(Desempenho *desempenho);
int ff_numero_medio_fragmentos(Desempenho *desempenho);

double bf_tempo_medio_alocacao(Desempenho *desempenho);
double wf_tempo_medio_alocacao(Desempenho *desempenho);
double nf_tempo_medio_alocacao(Desempenho *desempenho);
double ff_tempo_medio_alocacao(Desempenho *desempenho);

int bf_percentual_requisicao_negada(Desempenho *desempenho);
int wf_percentual_requisicao_negada(Desempenho *desempenho);
int nf_percentual_requisicao_negada(Desempenho *desempenho);
int ff_percentual_requisicao_negada(Desempenho *desempenho);

#endif // DESEMPENHO_H
