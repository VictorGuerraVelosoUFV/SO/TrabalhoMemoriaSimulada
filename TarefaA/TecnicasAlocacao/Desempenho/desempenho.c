/*
 * Copyright (c) 2018, Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the UFV nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "desempenho.h"

int inicializa_desempenho(Desempenho *desempenho) {

    desempenho->bf_fragmento = 0;
    desempenho->bf_contador = 0;
    desempenho->bf_negado = 0;

    desempenho->ff_fragmento = 0;
    desempenho->ff_contador = 0;
    desempenho->ff_negado = 0;

    desempenho->wf_fragmento = 0;
    desempenho->wf_contador = 0;
    desempenho->wf_negado = 0;

    desempenho->nf_fragmento = 0;
    desempenho->nf_contador = 0;
    desempenho->nf_negado = 0;
    return 0;

}

int bf_numero_medio_fragmentos(Desempenho *desempenho) {
    int fragmento = 0;
    int cont = 0;
    fragmento = desempenho->bf_fragmento;
    cont = desempenho->bf_contador;
    return fragmento / cont;
}

int wf_numero_medio_fragmentos(Desempenho *desempenho) {
    int fragmento = 0;
    int cont = 0;
    fragmento = desempenho->wf_fragmento;
    cont = desempenho->wf_contador;
    return fragmento / cont;

}

int nf_numero_medio_fragmentos(Desempenho *desempenho) {
    int fragmento = 0;
    int cont = 0;
    fragmento = desempenho->nf_fragmento;
    cont = desempenho->nf_contador;
    return fragmento / cont;

}

int ff_numero_medio_fragmentos(Desempenho *desempenho) {
    int fragmento = 0;
    int cont = 0;
    fragmento = desempenho->ff_fragmento;
    cont = desempenho->ff_contador;
    return fragmento / cont;

}

double bf_tempo_medio_alocacao(Desempenho *desempenho) {
    double tempo = 0;
    int cont = 0;
    tempo = desempenho->bf_tempo;
    cont = desempenho->bf_contador;
    return tempo / cont;

}

double wf_tempo_medio_alocacao(Desempenho *desempenho) {
    double tempo = 0;
    int cont = 0;
    tempo = desempenho->wf_tempo;
    cont = desempenho->wf_contador;
    return tempo / cont;
}

double nf_tempo_medio_alocacao(Desempenho *desempenho) {
    double tempo = 0;
    int cont = 0;
    tempo = desempenho->nf_tempo;
    cont = desempenho->nf_contador;
    return tempo / cont;
}

double ff_tempo_medio_alocacao(Desempenho *desempenho) {
    double tempo = 0;
    int cont = 0;
    tempo = desempenho->ff_tempo;
    cont = desempenho->ff_contador;
    return tempo / cont;
}

int bf_percentual_requisicao_negada(Desempenho *desempenho) {
    int cont = 0;
    int neg = 0;
    cont = desempenho->bf_contador;
    neg = desempenho->bf_negado;
    return neg / cont;
}

int wf_percentual_requisicao_negada(Desempenho *desempenho) {
    int cont = 0;
    int neg = 0;
    cont = desempenho->wf_contador;
    neg = desempenho->wf_negado;
    return neg / cont;
}

int nf_percentual_requisicao_negada(Desempenho *desempenho) {
    int cont = 0;
    int neg = 0;
    cont = desempenho->nf_contador;
    neg = desempenho->nf_negado;
    return neg / cont;
}

int ff_percentual_requisicao_negada(Desempenho *desempenho) {
    int cont = 0;
    int neg = 0;
    cont = desempenho->ff_contador;
    neg = desempenho->ff_negado;
    return neg / cont;
}
