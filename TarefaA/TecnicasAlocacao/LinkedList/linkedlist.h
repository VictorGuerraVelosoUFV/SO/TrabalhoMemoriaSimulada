#ifndef LINKED_LIST
#define LINKED_LIST
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../utils/utils.h"

typedef struct LLNode{
	void* data;
	struct LLNode* next;
	struct LLNode* previous;
}LLNode;

typedef struct{
	LLNode* first;
	LLNode* last;
	char dataType;
}LList;

int ll_initialize(LList** list, char dataType);
int ll_insert(LList* list, void** data);
int ll_remove(LList* list, void* data);
void ll_remove_aux(LList* list, LLNode** node);
void ll_print(LList* list);
#endif
