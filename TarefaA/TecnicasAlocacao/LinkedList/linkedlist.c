#include "linkedlist.h"

int ll_initialize(LList** list, char dataType){
	if ( ((*list) = MALLOC(LList,1) ) == NULL ) return -1;
	(*list)->dataType = dataType;
	if ( ((*list)->first = MALLOC(LLNode,1) ) == NULL ) return -1;
	(*list)->last = (*list)->first;
	(*list)->first->next = (*list)->first;
	(*list)->first->previous = (*list)->first;
	(*list)->first->data = NULL;
	return 0;
}

int ll_insert(LList* list, void** data){
	LLNode* newNode;
	if ( (newNode = MALLOC(LLNode,1) ) == NULL ) return -1;
	newNode->data = *data;
	newNode->next = list->last->next;
	list->last->next = newNode;
	newNode->previous = list->last;
	list->last = newNode;

	return 0;
}

int ll_remove(LList* list, void* data){
	if(list->first == list->last) return 1;

	LLNode* current = list->first->next;

	while(current != list->first){
		if(current->data == data){
			ll_remove_aux(list, &current);
			return 0;
		}
		current = current->next;
	}

	return -1;
}

void ll_remove_aux(LList* list, LLNode** node){
	if(*node == list->last){
		list->last = (*node)->previous;
		(*node)->previous->next = NULL;
		free((*node));
		return;
	}
	(*node)->previous->next = (*node)->next;
	(*node)->next->previous = (*node)->previous;
	free((*node));
}

void ll_print(LList* list){
	if(list->first == list->last){
		printf("Empty list\n");
		return;
	}

	LLNode* current = list->first->next;

	switch(list->dataType){
		case 's':
			while(current != NULL){
				printf("%s\n", (char*) current->data);
				current = current->next;
			}
			break;
		case 'd':
			while(current != NULL){
				printf("%d\n", *(int*) current->data);
				current = current->next;
			}
			break;
		case 'f':
			while(current != NULL){
				printf("%.4f\n", *(float*) current->data);
				current = current->next;
			}
			break;
	}
}
