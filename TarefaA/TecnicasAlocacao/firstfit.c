/*
 * Copyright (c) 2018, Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *	   * Redistributions of source code must retain the above copyright
 *	   notice, this list of conditions and the following disclaimer.
 *	   * Redistributions in binary form must reproduce the above copyright
 *	   notice, this list of conditions and the following disclaimer in the
 *	   documentation and/or other materials provided with the distribution.
 *	   * Neither the name of the UFV nor the
 *	   names of its contributors may be used to endorse or promote products
 *	   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "firstfit.h"

void ff_inicializa_memoria(ff_Memoria **memoria) {
    // Inicializar unidade de memória
    UnidadeMemoria *unit;
    //Inicializar memoria
    *memoria = MALLOC(ff_Memoria, 1);
    ll_initialize(&(*memoria)->espacoEnderecamento, 's');
    for (int i = 0; i < MEM_SIZE; i++) {
        um_inicializar(&unit);
        ll_insert((*memoria)->espacoEnderecamento, (void **) &unit);
    }
    (*memoria)->size = 256;
    (*memoria)->free = (*memoria)->size;
}

int ff_aloca_memoria(ff_Memoria *memoria, int pid, int numUnits, Desempenho *desempenho) {
    unsigned livres = 0;
    UnidadeMemoria *unit = NULL;
    LLNode *currNode = memoria->espacoEnderecamento->first->next;
    LLNode *initialNode = currNode;
    double time_taken;
    int fragmento;
    desempenho->ff_contador++;
    clock_t t;
    int res = -1;
    t = clock();
    /*Inicio do Algoritmo*/
    while (currNode !=
           memoria->espacoEnderecamento->first) {                                                 // enquanto não tiver percorrido os 256 nodos
        int sizee;

        unit = (UnidadeMemoria *) currNode->data;                             // pega-se a unidade de memória do nodo atual

        if (unit->empty) {                                                     // verifica se está vazia
            livres++;                                                        // se estiver aumenta o contador de vazias
            if (livres ==
                numUnits) {                                          // se o contador for igual o número necessário para realizar a alocação
                unit = (UnidadeMemoria *) initialNode->data;
                um_alocar(unit, pid);
                sizee = memoria->size;
                fragmento = sizee - numUnits;
                desempenho->ff_fragmento = fragmento;
                printf("\nfragmento: %d\n", fragmento);
                do {                                                          // preenche a memoria do inicial até o atual
                    initialNode = initialNode->next;
                    unit = (UnidadeMemoria *) initialNode->data;
                    um_alocar(unit, pid);
                } while (initialNode != currNode);
                res = 0;
                break;
            }
        } else {
            livres = 0;                                                      // a sequência de unidades livres consecutivas é resetada
            initialNode = currNode->next;

            // a proxima unidade depois da atual é a nova inicial
        }
        currNode = currNode->next;

    }
    t = clock() - t;
    time_taken = ((double) t) / (CLOCKS_PER_SEC/1000);
    desempenho->ff_tempo += time_taken;
    printf("\nTempo: %lf\n", desempenho->ff_tempo);
    /*Fim do Algoritmo*/

    return res;
}

int ff_libera_memoria(ff_Memoria *memoria, int pid) {
    LLNode *currNode = memoria->espacoEnderecamento->first->next;
    UnidadeMemoria *unit;

    while (currNode != NULL) {
        unit = (UnidadeMemoria *) currNode->data;
        if (unit->pid == pid) {
            do {
                unit->pid = -1;
                unit->empty = true;
                currNode = currNode->next;
                unit = (UnidadeMemoria *) currNode->data;
            } while (unit->pid == pid);
            return 1;
        }
        currNode = currNode->next;
    }
    return -1;
}

void ff_imprime_memoria(ff_Memoria *memoria) {
    int linebreak = 0;
    UnidadeMemoria *unit;
    LLNode *nodo = memoria->espacoEnderecamento->first->next;

    for (int i = 0; i < 16; i++) {
        printf("%d\t", i);
    }
    printf("\n");
    while (nodo != memoria->espacoEnderecamento->first) {
        unit = (UnidadeMemoria *) nodo->data;
        if (linebreak <= 15) {
            printf("%d\t", unit->pid);
        } else {
            printf("\n%d\t", unit->pid);
            linebreak = 0;
        }
        linebreak++;
        nodo = nodo->next;
    }
    printf("\n");
}
