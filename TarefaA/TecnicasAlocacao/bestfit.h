/*
 * Copyright (c) 2018, Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the UFV nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BESTFIT_H
#define BESTFIT_H
#define MEM_SIZE 256
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include "LinkedList/linkedlist.h"
#include "UnidadeMemoria/unidadememoria.h"
#include "Desempenho/desempenho.h"
#define min(a, b) (((a) < (b)) ? (a) : (b))

typedef struct bf_Pair{
    int key;    //size
    LLNode* value; //initial pos
}bf_Pair;
typedef struct bf_Memoria{
	LList* espacoEnderecamento;
	int size;   //Tamanho da memória
	bool free;   //Espaço livre total
}bf_Memoria;

void bf_insertion_sort(bf_Pair* array, unsigned int size);
void bf_inicializa_memoria(bf_Memoria** memoria);
void bf_aloca_espaco(bf_Pair *espacos, int idx, UnidadeMemoria *unit, int pid, int numUnits);
int bf_aloca_memoria(bf_Memoria *memoria, int pid, int numUnits,Desempenho *desempenho);
int bf_libera_memoria(bf_Memoria *memoria, int pid);
void bf_imprime_memoria(bf_Memoria *memoria);

#endif // BESTFIT_H
