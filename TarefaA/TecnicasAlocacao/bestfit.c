/*
 * Copyright (c) 2018, Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the UFV nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "bestfit.h"

void bf_insertion_sort(bf_Pair *array, unsigned int size) {
    int i, j;
    bf_Pair current;

    for (j = 1; j < size; j++) {
        current = array[j];
        i = j - 1;

        while (i >= 0 && array[i].key > current.key) {
            array[i + 1] = array[i];
            i--;
        }
        array[i + 1] = current;
    }
}

void bf_inicializa_memoria(bf_Memoria **memoria) {
    // Inicializar unidade de memória
    UnidadeMemoria *unit;
    //Inicializar memoria
    *memoria = MALLOC(bf_Memoria, 1);
    ll_initialize(&(*memoria)->espacoEnderecamento, 's');
    for (int i = 0; i < MEM_SIZE; i++) {
        um_inicializar(&unit);
        ll_insert((*memoria)->espacoEnderecamento, (void **) &unit);
    }
    (*memoria)->size = 256;
    (*memoria)->free = (*memoria)->size;
}

void bf_aloca_espaco(bf_Pair *espacos, int idx, UnidadeMemoria *unit, int pid, int numUnits) {
    LLNode *currentNode = espacos[idx].value;
    for (int i = 0; i < min(espacos[idx].key, numUnits); i++) {
        unit = (UnidadeMemoria *) currentNode->data;
        currentNode = currentNode->next;
        um_alocar(unit, pid);
    }

}

int bf_aloca_memoria(bf_Memoria *memoria, int pid, int numUnits, Desempenho *desempenho) {
    unsigned livres = 0;
    int nodosPercorridos = 0;
    int livre = 0;
    int fragmento = 0;
    int sizee;
    int res = -1;
    double time_taken;
    desempenho->bf_contador++;
    printf("\nContador: %d\n", desempenho->bf_contador);
    UnidadeMemoria *unit = NULL;
    LLNode *currNode;
    LLNode *initialNode = memoria->espacoEnderecamento->first->next;
    bf_Pair *espacos = MALLOC(bf_Pair, memoria->size); //vetor de tamanho dos espacos vazios
    int size = 0; //tamanho do vetor
    clock_t t;
    t = clock();
    for (currNode = initialNode; currNode != memoria->espacoEnderecamento->first; currNode = currNode->next) {
        unit = (UnidadeMemoria *) currNode->data;
        if (unit->empty && currNode != memoria->espacoEnderecamento->last) livre++;
        else {
            if (livre > 0) {
                size++;
                espacos[size - 1].key = livre;
                espacos[size - 1].value = initialNode;
                if (livre == numUnits) {
                    res = 0;
                    break;
                }
                livre = 0;
            }
            initialNode = currNode->next;
        }
    }

    sizee = memoria->size;
    fragmento = sizee - numUnits;
    printf("\nfragmento: %d\n", fragmento);
    desempenho->bf_fragmento = fragmento;
    bf_insertion_sort(espacos, (unsigned) size); //Ordena
    for (int i = 0; i < size; i++) {
        if (espacos[i].key >= numUnits) { //Encontra Mínimo Item Suficiente
            bf_aloca_espaco(espacos, i, unit, pid, numUnits);// aloca no espaço encontrado
            res = 0;
            break;
        }
    }
    t = clock() - t;
    time_taken = ((double) t) / (CLOCKS_PER_SEC/1000);
    desempenho->bf_tempo += time_taken;
    printf("\nTempo: %lf\n", desempenho->bf_tempo);
    printf("\n%d\n", desempenho->bf_contador);
    free(espacos);
    return res;
}

int bf_libera_memoria(bf_Memoria *memoria, int pid) {
    LLNode *currNode = memoria->espacoEnderecamento->first->next;
    UnidadeMemoria *unit;

    while (currNode != NULL) {
        unit = (UnidadeMemoria *) currNode->data;
        if (unit->pid == pid) {
            do {
                unit->pid = -1;
                unit->empty = true;
                currNode = currNode->next;
                unit = (UnidadeMemoria *) currNode->data;
            } while (unit->pid == pid);
            return 1;
        }
        currNode = currNode->next;
    }
    return -1;
}

void bf_imprime_memoria(bf_Memoria *memoria) {
    int linebreak = 0;
    UnidadeMemoria *unit;
    LLNode *nodo = memoria->espacoEnderecamento->first->next;

    for (int i = 0; i < 16; i++) {
        printf("%d\t", i);
    }
    printf("\n");
    while (nodo != memoria->espacoEnderecamento->first) {
        unit = (UnidadeMemoria *) nodo->data;
        if (linebreak <= 15) {
            printf("%d\t", unit->pid);
        } else {
            printf("\n%d\t", unit->pid);
            linebreak = 0;
        }
        linebreak++;
        nodo = nodo->next;
    }
    printf("\n");
}
