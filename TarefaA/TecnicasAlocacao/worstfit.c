/*
 * Copyright (c) 2018, Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the UFV nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Athena (athenasaran@gmail.com), Victor (victorgvbh@gmail.com) and Ruan (ruanformigoni@gmail.com) BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "worstfit.h"

void wf_insertion_sort(wf_Pair *array, unsigned int size) {
    int i, j;
    wf_Pair current;

    for (j = 1; j < size; j++) {
        current = array[j];
        i = j - 1;

        while (i >= 0 && array[i].key > current.key) {
            array[i + 1] = array[i];
            i--;
        }
        array[i + 1] = current;
    }
}

void wf_inicializa_memoria(wf_Memoria **memoria) {
    // Inicializar unidade de memória
    UnidadeMemoria *unit;
    //Inicializar memoria
    *memoria = MALLOC(wf_Memoria, 1);
    ll_initialize(&(*memoria)->espacoEnderecamento, 's');
    for (int i = 0; i < MEM_SIZE; i++) {
        um_inicializar(&unit);
        ll_insert((*memoria)->espacoEnderecamento, (void **) &unit);
    }
    (*memoria)->size = 256;
    (*memoria)->free = (*memoria)->size;
}

void wf_aloca_espaco(wf_Pair *espacos, int idx, UnidadeMemoria *unit, int pid, int numUnits) {
    LLNode *currentNode = espacos[idx].value;
    for (int i = 0; i < min(espacos[idx].key, numUnits); i++) {
        unit = (UnidadeMemoria *) currentNode->data;
        currentNode = currentNode->next;
        um_alocar(unit, pid);
    }
}

int wf_aloca_memoria(wf_Memoria *memoria, int pid, int numUnits, Desempenho *desempenho) {
    unsigned livres = 0;
    double time_taken;
    clock_t t;
    int nodosPercorridos;
    UnidadeMemoria *unit = NULL;
    int sizee;
    sizee = memoria->size;
    desempenho->wf_contador++;
    LLNode *currNode = memoria->espacoEnderecamento->first->next;
    LLNode *initialNode = currNode;
    int fragmento = 0;
    int res = -1;
    wf_Pair *espacos = MALLOC(wf_Pair, memoria->size); //vetor de tamanho dos espacos vazios
    espacos[0].key = 1; //tamanho do vetor
    /*Inicio do Algoritmo*/
    sizee = memoria->size;
    fragmento = sizee - numUnits;
    desempenho->wf_fragmento = fragmento;
    printf("\nfragmento: %d\n", fragmento);
    t = clock();
    while (currNode !=
           memoria->espacoEnderecamento->first) {                                                 // enquanto não tiver percorrido os 256 nodos


        nodosPercorridos = 1;
        unit = (UnidadeMemoria *) currNode->data;                             // pega-se a unidade de memória do nodo atual
        if (!unit->empty || currNode ==
                            memoria->espacoEnderecamento->last) {                                                     // verifica se não está vazia
            size_t sz = ++espacos[0].key;
            espacos[espacos[0].key - 1].key = livres; //Registra tamanho do ultimo espaco
            espacos[espacos[0].key - 1].value = initialNode;
            livres = 0;                                                      // a sequência de unidades livres consecutivas é resetada
            initialNode = currNode->next;                                    // a proxima unidade depois da atual é a nova inicial
        } else livres++;
        currNode = currNode->next;
        nodosPercorridos++;
    }
    wf_insertion_sort(espacos + 1, (unsigned) espacos[0].key); //Ordena
    for (int i = (unsigned) espacos[0].key; i > 0; i--) {
        if (espacos[i].key >= numUnits) { //Encontra maximo item suficiente
            currNode = espacos[i].value;
            wf_aloca_espaco(espacos, i, unit, pid, numUnits);
            res = 0;
        }
    }

    /*Fim do Algoritmo*/
    t = clock() - t;
    time_taken = ((double) t) / (CLOCKS_PER_SEC / 1000);
    desempenho->wf_tempo += time_taken;
    free(espacos);
    return res;
}

int wf_libera_memoria(wf_Memoria *memoria, int pid) {
    LLNode *currNode = memoria->espacoEnderecamento->first->next;
    UnidadeMemoria *unit;

    while (currNode != NULL) {
        unit = (UnidadeMemoria *) currNode->data;
        if (unit->pid == pid) {
            do {
                unit->pid = -1;
                unit->empty = true;
                currNode = currNode->next;
                unit = (UnidadeMemoria *) currNode->data;
            } while (unit->pid == pid);
            return 1;
        }
        currNode = currNode->next;
    }
    return -1;
}

void wf_imprime_memoria(wf_Memoria *memoria) {
    int linebreak = 0;
    UnidadeMemoria *unit;
    LLNode *nodo = memoria->espacoEnderecamento->first->next;

    for (int i = 0; i < 16; i++) {
        printf("%d\t", i);
    }
    printf("\n");
    while (nodo != memoria->espacoEnderecamento->first) {
        unit = (UnidadeMemoria *) nodo->data;
        if (linebreak <= 15) {
            printf("%d\t", unit->pid);
        } else {
            printf("\n%d\t", unit->pid);
            linebreak = 0;
        }
        linebreak++;
        nodo = nodo->next;
    }
    printf("\n");
}
